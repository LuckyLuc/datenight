# Date Night

You are controlling 2 parents whose goal is to get out of the house in X number of moves, making sure their kids are taken 
care of and ready for the babysitter.

### Environment

Environment is a top-down view of a multi-room house

### Objective

Objective can vary based on baby types and possibly pets, babysitter type, but in general, __fed and clean babies get put into the living room with the babysitter__ is the victory condition. 

### Assets
 
There is a collection of supplies which do different things, which will be needed depending on the environmental challenge. Examples:

  * DVD (distracts a mobile baby)
  * Diapers (cleans a dirty baby, keeps it clean for n turns)
  * Bottle (feeds a hungry baby and keeps it fed for n turns)
  * Baby gate (closes off a doorway for all PCs and NPCs, can be moved)
  * Cat food (keep the cat from clawing up the baby)
  * Play pen (box a baby in without closing off the room)

### Babysitter
Different babysitters have different characteristics:

  * Grandmother - always holds a crying baby, but won't change diapers, turn penalty if she is in a room with a parent and no baby 
  * Neighbor Teenager - Will only change a dirty diaper, but won't hold baby
  * Big brother? - Must be bribed? 
  * Others?

### Babies
A baby's outfit color will determine its behaviors:

  * Green - babies are always crawling toward the nearest door (will go to the next open room or back the other way if it's the last room and doesn't have a game over condition)
  * Yellow - will need a diaper change every n turns
  * Red - will need fed every n turns
  * Blue - must always be held or they will cry
  * Orange - Always climbs the closest furniture (some furniture can be safe)
  * Purple - has a cold, needs medicine

### Obstacles

  * Rooms will have known obstacles
    * kitchen can burn baby (game over)
    * bathroom has a tub, which drowns baby (game over)
    * living room has bookcases to climb (game over)
    * bedroom has a cat that scratches baby (game over)
  * Other obstacles:
    * Medicine cabinet needs key
    * Limit to items you can carry
    * Only dad can operate play pen or DVD
    * Only mom can feed or change baby

### Turns

Moves are point and click. NPCs all update after each parent turn. 

__Scene:__ A 3-room house, kitchen  with door to dining room, which leads to living room. A red, green, and yellow baby, plus mom and dad are in the living room. Grandma is in her car outside the kitchen door. 

1. Player: Click the mom, click the red baby, click the bottle. Mom feeds the hungry baby. 
2. NPC: Now the diaper baby gets wetter (closer to diaper rash - a losing condition), the mobile baby enters the dining room, grandma exits her car and goes to the kitchen door outside. 
3. Player: click dad, click baby gate, click dining room/kitchen door. 
4. NPC: Mobile baby heads to kitchen but is boxed in, and the wet baby is near-critical. Red baby begins feed cycle (n turns)Grandma enters kitchen, but is stopped by the baby gate (no turn lost to conversation) 
5. Player: Click mom, click diaper, click yellow baby. Mom changes the wet baby. 
6. NPC: Mobile baby enters living room. Grandma stays put, Red baby progresses cycle.
7. Player: Click dad, click DVD, click TV. 
8. NPC: Mobile baby will stay in living room. 
9. Player: Now parents have to let grandma in and leave before red and yellow cycle through again. Click mom, click kitchen/dining gate, click dining room. 
10. NPC: Gate is up, so grandma goes to living room with babies, victory.

That's 5 player turns, but it can be done in 4 without the DVD or gate for 3 stars:

1. Dad picks up the blue baby
2. Baby goes nowhere, grandma exits car
3. mom feeds Red
4. Grandma comes inside, wet baby near-crit
5. Dad continues holding baby
6. Grandma enters dining room
7. Mom changes yellow
8. Grandma in the living room with the babies, victory


### Victory 

Tiers of victory:

* Pass before max turns or supplies run out (date success)
* Pass with the fewest possible items used to win (happy dad) 
* Fewest possible turns taken (happy mom)
* Both parents happy (perfect date - 3 stars)


### Difficulty

Difficulty would be set by starting environment, number of babies, rooms, supplies, obstacles, etc. 


### Level generation

* Manual at first
* Create a level generating algorithm with inputs for complexity
* User-developed levels

### Skins

This could easily be skinned to be an Indiana Jones (knock off) game with types of babies being a rat running around (mobile baby that needs trapping, cheese for delay), bear trap that needs a rock dropped in it (a red baby that needs a bottle), and a timed lock that causes the ceiling to collapse (a baby that needs a diaper change). Characters could be Kentucky Smith and Tall Jagged (Indy and Short Round), the house layout is the archaeological site being raided, grandma/grandpa are Nazis. The skinning possibilities are wildly varied. Old West, Jewel Heist, etc. Could even make different skins as different versions, unlocks, etc. 